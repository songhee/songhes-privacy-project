package com.test.frag3test;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private final int FiRSTFRAGMENT =0;
    private final int SECONDFRAGMENT =1;
    private final int THIRDFRAGMENT = 2;

    FragmentTwo fragment = (FragmentTwo) getFragmentManager().findFragmentById(R.id.textView2);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeIntoFrag(FiRSTFRAGMENT);
    }

    public void changeIntoFrag(int idx) {
        Fragment fr = null;
        switch (idx){
            case FiRSTFRAGMENT:
                fr = new FragmentOne();
                break;
            case SECONDFRAGMENT:
                fr = new FragmentTwo();
                break;
            case THIRDFRAGMENT:
                fr = new FragmentThree();
                break;
            default:
                break;
        }
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fr);
        fragmentTransaction.commit();

    }

    public void selectFrag(View view) {

        if(view == findViewById(R.id.button3)) {
            changeIntoFrag(THIRDFRAGMENT);
        }
        else if(view == findViewById(R.id.button2)) {
            changeIntoFrag(SECONDFRAGMENT);
        }
        else {
            changeIntoFrag(FiRSTFRAGMENT);
        }
    }


}