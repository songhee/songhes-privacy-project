package com.test.frag3test;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by hsh on 2015-12-09.
 */
public class LottoNumberExtractor {

    // 7개의 번호가 담길 배열
    int[] num = new int[6];

    public int[] getNum() {
        return num;
    }


    public void RandomNumGenrator() {

        Random rnd = new Random();
        int cnt = 0;
        int n;

        jump:
        while(cnt<6)
        {
            n = rnd.nextInt(45)+1; // 1~45사이의 난수 발생
            for (int i=0; i<cnt; i++)
            {
                if (num[i]==n)
                    continue jump;
            }
            num[cnt++]=n;        //-- 비로소 담아준다. (같은 수가 아닐 때)
        }
        sorting();

    }

    private void sorting()
    {
        Arrays.sort(num);    // Array클래스에서 제공하는 sort 메소드 사용
    }


}


