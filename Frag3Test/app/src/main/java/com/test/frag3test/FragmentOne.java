package com.test.frag3test;

/**
 * Created by hsh on 12/17/2015.
 */
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentOne extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState ) {

        View v = inflater.inflate(R.layout.fragment_one, container, false);

        final TextView[] lottoNum =new TextView[6];
        lottoNum[0]= (TextView)v.findViewById(R.id.lottoNum1);
        lottoNum[1] = (TextView)v.findViewById(R.id.lottoNum2);
        lottoNum[2] = (TextView)v.findViewById(R.id.lottoNum3);
        lottoNum[3] = (TextView)v.findViewById(R.id.lottoNum4);
        lottoNum[4] = (TextView)v.findViewById(R.id.lottoNum5);
        lottoNum[5] = (TextView)v.findViewById(R.id.lottoNum6);

        final Button btn = (Button)v.findViewById(R.id.btn_test);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LottoNumberExtractor lotto = new LottoNumberExtractor();
                lotto.RandomNumGenrator();

                int[] tempNum = lotto.getNum();
                for(int n=0; n<6; n++) {
                    Log.d("test", "num:" + tempNum[n]);
                    lottoNum[n].setText(Integer.toString(tempNum[n]));
                }

            }
        });

        //Inflate the layout for this fragment
        return v;
       /* return inflater.inflate(
                R.layout.fragment_one, container, false);*/
    }
}