package com.test.frag3test;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by hsh on 12/17/2015.
 */
public class FragmentThree extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_three, container, false);

        final Button devideButton = (Button) view.findViewById(R.id.devideBtn);


        devideButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final EditText mEdit = (EditText) view.findViewById(R.id.edit_message);
                final TextView toPayMoney = (TextView) view.findViewById(R.id.toPayMoney);
                final int money = Integer.parseInt(mEdit.getText().toString());
                // Integer.parseInt(mEdit.getText().toString());
                int devideMoney;
                devideToThree d = new devideToThree();
                devideMoney = d.deviedToThree(money);
                toPayMoney.setText(Integer.toString(devideMoney));
            }
        });

        //RadioButton radioBtn = (RadioButton) view;
        String resultText = "";
        final TextView tv = (TextView) view.findViewById(R.id.paidPerson);
        view.findViewById(R.id.radioButton).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                tv.setText("김선아/농협/71102132265");
                /*printChecked(v);*/
            }
        });
        view.findViewById(R.id.radioButton2).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                tv.setText("이혜정/신한/110194367517");
                /*printChecked(v);*/
            }
        });
        view.findViewById(R.id.radioButton3).setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
                tv.setText("허송희/국민/67690201518183");
                /*printChecked(v);*/
            }
        });



        return view;
    }

/*    public void printChecked(View v) {
        *//*final View view = inflater.inflate(R.layout.fragment_three, container, false);*//*


        RadioButton radioBtn = (RadioButton) v;
        String resultText = "";
        TextView tv = (TextView) view.findViewById(R.id.paidPerson);
        if (radioBtn.isChecked()) {
            if (radioBtn.getText().toString().equals("김선아")) {
                tv.setText("김선아/농협/71102132265");
            } else if (radioBtn.getText().toString().equals("이혜정")) {
                tv.setText("이혜정/신한/110194367517");
            } else {
                tv.setText("허송희/국민/67690201518183");
            }

        }
    }*/
    // Inflate the layout for this fragment



}


